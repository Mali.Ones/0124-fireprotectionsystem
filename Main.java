class Main {
    
    /**
     * Main -metode for å teste FireProtectionSystem -classen
     * Det som foregår her simulerer alle "input" til
     * systemet.
     * 
     * @param args
     */
    public static void main(String[] args) {
        // Oppretter et nytt objekt
        FireProtectionSystem system = new FireProtectionSystem();

        // Installere sensorer
        // Lager en array { null, null, null, null }
        Sensor[] mySensors = new Sensor[4]; 
        for (int i = 0; i < mySensors.length; i++) {
            // Oppretter nytt Sensor -objekter og putter det inn i arrayen
            mySensors[i] = new Sensor();
        }
        system.installSensor(mySensors);

        // Installere alarmer
        Alarm myAlarm = new Alarm();  // Oppretter et nytt Alarm -objekt
        system.installAlarm(myAlarm);

        // Oppdag røyk
        mySensors[0].setSmokeDetected(true);
        mySensors[1].setSmokeDetected(true);

        // Fjern røyk
        mySensors[0].setSmokeDetected(false); // Alarmen skal ikke gå av enda, men
        mySensors[1].setSmokeDetected(false); // Nåå forventer vi at alarmen går av
    }
}